#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define NB_READS 1000000
//#define NB_RUNS 1
//#define NBYTES 4096


size_t offsets[NB_READS];
int nbytes, nb_runs;
double run_time, total_bytes, throughput, ops;
struct timespec begin, end;
size_t modulo, file_len;
char *output;
int fd, fdw;

void init_size(const char *path){
printf("Creating output file\n");
  fdw = open("output", O_RDWR);
  
  if(fdw == -1){
    perror("Opening output file");
    return;
  }
 
  // Getting input file length
  FILE *fp = fopen(path, "rb");
  fseek(fp, 0, SEEK_END);
  file_len = ftell(fp);
  fclose(fp);  
  printf("Input file length : %ld MB\n\n",file_len/1024/1024); 
 
  //-----

  modulo = file_len<nbytes ? file_len : file_len-nbytes;
  
  srand(time(NULL));
  
  //--- Open file ---

  fd = open(path, O_RDWR);
  output = malloc(file_len);
  if(output == NULL){
    perror("malloc");
    return;
  }  
}

void init_rand(){
  size_t offset_max = 0;
  printf("\n\nGenerating random offsets...");
  for(int i=0; i<NB_READS; i++){
    offsets[i] = rand()%(modulo);
    if(offsets[i]>offset_max){
      offset_max=offsets[i];
    }
  }
  printf("OK. \n");
  printf("Read area : %f MB\n\n", (double)offset_max/1024/1024);  
}


void rand_reads(){
  printf("------------------------------------\n");
  printf("|   Random Read performance test   |\n");
  printf("------------------------------------\n\n");
  printf("Start reading...\n\n");

  clock_gettime(CLOCK_MONOTONIC, &begin); 

  for(int j=0; j<nb_runs; j++){
  
    for(int i=0; i<NB_READS; i++){
      pread(fd, output+offsets[i], nbytes, offsets[i]); // keep a copy in the "output" buffer
    }
    
  }
  clock_gettime(CLOCK_MONOTONIC, &end);
  run_time = (double)((end.tv_sec-begin.tv_sec)+(abs(end.tv_nsec-begin.tv_nsec)/1000000000.0));
  printf("Execution time : %f\n", run_time);
  total_bytes = (double)nbytes*nb_runs*NB_READS;
  throughput = (total_bytes/run_time);
  ops=(nb_runs*NB_READS)/run_time;
  printf("Throughput : %d  MB/s\n", (int)(throughput/1024/1024)); 
  printf("OPS : %d\n", (int)ops);
  printf("Avg Latency : %f µs\n\n\n", 1000000/ops);
}

void rand_writes(){  
  printf("-------------------------------------\n");
  printf("|   Random Write performance test   |\n");
  printf("-------------------------------------\n\n");
  printf("Start writing...\n\n");
  
  
  clock_gettime(CLOCK_MONOTONIC, &begin);
  for(int j = 0; j<nb_runs; j++){
    for(int i = 0; i<NB_READS; i++){
      //printf("Writing fd=%d nbyte=%d offset=%ld\n", fd, NBYTES, offsets[i]);
      int test = pwrite(fdw, output+offsets[i], nbytes, offsets[i]);
      if(test!=nbytes){
	perror("pwrite");
	return;
      }
    }
  }
  clock_gettime(CLOCK_MONOTONIC, &end);
  total_bytes = (double)nbytes*nb_runs*NB_READS;
  run_time = (double)((end.tv_sec-begin.tv_sec)+(abs(end.tv_nsec-begin.tv_nsec)/1000000000.0));
  throughput = (total_bytes/run_time);
  ops=(nb_runs*NB_READS)/run_time;
  printf("Execution time : %f \n", run_time);
  printf("Throughput : %d MB/s\n", (int)(throughput/1024/1024));
  printf("OPS : %d\n", (int)ops);  
  printf("Avg Latency : %f µs\n\n\n", 1000000/ops);
}


void init_seq(){
  printf("Generating sequential offsets...\n");
  for(int i=0; i<NB_READS; i++){
    offsets[i] = (i*nbytes)%modulo;
  }
}


void seq_reads(){
 printf("------------------------------------\n");
  printf("| Sequential Read performance test |\n");
  printf("------------------------------------\n\n");
  printf("Start reading...\n\n");

  clock_gettime(CLOCK_MONOTONIC, &begin); 
  for(int j=0; j<nb_runs; j++){
    for(int i=0; i<NB_READS; i++){
      pread(fd, output+offsets[i], nbytes, offsets[i]); // keep a copy in the "output" buffer
    }
    
  }
  clock_gettime(CLOCK_MONOTONIC, &end);
  run_time = (double)((end.tv_sec-begin.tv_sec)+(abs(end.tv_nsec-begin.tv_nsec)/1000000000.0));
  printf("Execution time : %f\n", run_time);
  total_bytes = (double)nbytes*nb_runs*NB_READS;
  throughput = (total_bytes/run_time);
  ops=(nb_runs*NB_READS)/run_time;
  printf("Throughput : %d  MB/s\n", (int)(throughput/1024/1024)); 
  printf("OPS : %d\n", (int)ops);
  printf("Avg Latency : %f µs\n\n\n", 1000000/ops);
}


void seq_writes(){
  
  printf("-------------------------------------\n");
  printf("| Sequential Write performance test |\n");
  printf("-------------------------------------\n\n");
  printf("Start writing...\n\n");
  
  
  clock_gettime(CLOCK_MONOTONIC, &begin);
  for(int j = 0; j<nb_runs; j++){
    for(int i = 0; i<NB_READS; i++){
      int test = pwrite(fdw, output+offsets[i], nbytes, offsets[i]);
      if(test!=nbytes){
	perror("pwrite");
	printf("SIZE OF FILE : %ld   MODULO : %ld\n", file_len, modulo);
	return;
      }
    }
  }
  clock_gettime(CLOCK_MONOTONIC, &end);
  total_bytes = (double)nbytes*nb_runs*NB_READS;
  run_time = (double)((end.tv_sec-begin.tv_sec)+(abs(end.tv_nsec-begin.tv_nsec)/1000000000.0));
  throughput = (total_bytes/run_time);
  ops=(nb_runs*NB_READS)/run_time;
  printf("Execution time : %f \n", run_time);
  printf("Throughput : %d MB/s\n", (int)(throughput/1024/1024));
  printf("OPS : %d\n", (int)ops);  
  printf("Avg Latency : %f µs\n\n\n", 1000000/ops);
}







int main(int argc, char **argv){

  if(argc != 4){
    printf("Usage ; %s <path> <nbytes> <nb_runs>\n", argv[0]);
    return EXIT_FAILURE;
  }

  nbytes = atoi(argv[2]);
  nb_runs = atoi(argv[3]);
  init_size(argv[1]);

  #ifdef SR
  init_seq();
  seq_reads();
  #endif

  #ifdef SW
  init_seq();
  seq_writes();
  #endif

  #ifdef RR
  init_rand();
  rand_reads();
  #endif

  #ifdef RW
  init_rand();
  rand_writes();
  #endif
  
  /*printf("Closing input file...\n");
  close(fd);
  printf("Closing output file...\n");
  close(fdw);
  */
  return EXIT_SUCCESS;
}
