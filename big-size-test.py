import os
import subprocess
import re


input_file="input_file_1G"
output_dir="bigsizetest-nvcache"
benchs=["sw", "sr", "rr", "rw"]

tp, ops, avglat = '', '', ''

os.system("mkdir "+output_dir)


for bench in benchs:
    file = open(output_dir+"/"+bench+"-size", "w+")
    for i in range(1, 16):
        size=4096+128*i*2
        print("Bench : "+bench+" size="+str(size))
        
        res = subprocess.check_output("numactl --cpunodebind=0 ./"+bench+"-test "+input_file+" "+str(size)+" 10", shell=True)
        res=res.decode('utf-8').split("\n")
        for line in res:
            if "Throughput" in line:
                tp=line.split(" ")[2]
            if "OPS" in line:
                ops=line.split(" ")[2]
            if "Avg Latency" in line:
                avglat=line.split(" ")[3]

        print(tp+" "+ops+" "+avglat)
        file.write(str(size)+" "+tp+" "+ops+" "+avglat+"\n")

file.close()
    
