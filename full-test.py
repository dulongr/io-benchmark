import os
import subprocess
import re

nbtests = 5

input_file="input_file_100M"
output_dir="results-nvcache-smaller"
benchs=["sw", "sr", "rr", "rw"]


tp, ops, avglat = '', '', ''

os.system("mkdir "+output_dir)


for bench in benchs:
    file = open(output_dir+"/"+bench, "w+")
    for i in range(nbtests):
        print("Bench : "+bench)
        
        res = subprocess.check_output("numactl --cpunodebind=0 ./"+bench+"-test "+input_file, shell=True)
        res=res.decode('utf-8').split("\n")
        for line in res:
            if "Throughput" in line:
                tp=line.split(" ")[2]
            if "OPS" in line:
                ops=line.split(" ")[2]
            if "Avg Latency" in line:
                avglat=line.split(" ")[3]

        print(tp+" "+ops+" "+avglat)
        file.write(tp+" "+ops+" "+avglat+"\n")

file.close()
    
