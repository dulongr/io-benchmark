SRC=main.c
CFLAGS=-g -O0 -Wall


all: 
	$(CC) $(CFLAGS) $(SRC) -o sr-test -D SR
	$(CC) $(CFLAGS) $(SRC) -o sw-test -D SW
	$(CC) $(CFLAGS) $(SRC) -o rr-test -D RR
	$(CC) $(CFLAGS) $(SRC) -o rw-test -D RW


clean:
	rm *.o sr-test sw-test rr-test rw-test
